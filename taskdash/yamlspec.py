import yaml

def parse(raw_yaml):
    return yaml.safe_load(raw_yaml)
